import React from 'react';
import { connect } from 'react-redux';
import {addictivePlayerItems, initPlayerItemsHandler} from './playerFunctions';
import {addictiveVehicleItems, initVehicleItemsHandler, lastActionNull} from './vehicleFunctions';

class Interaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        
        };
    }
    
    componentDidMount() {
        var playerItems = [
            {
                text: "Познакомиться",
                icon: "handshake.png"
            },
            {
                text: "Обмен",
                icon: "trade.png"
            },
            {
                text: "Показать документы",
                icon: "card.png"
            },
        ];
        var vehicleItems = [{
            text: "Заправить"
        }, ];
        window.interactionMenuAPI = {
            showPlayerMenu: (data = null) => {
                data = JSON.parse(data);
                $("#interactionMenu").empty();
                for (var i = 0; i < playerItems.length; i++) {
                    var info = playerItems[i];
                    var iconName = (info.icon) ? info.icon : "default.png";
                    var itemEl = $(`<div class="interaction_item"><div class="icon"><img src="img/interactionMenu/${iconName}"/>
                    </div><div class="text">${info.text}</div></div>
                `);
                    $("#interactionMenu").append(itemEl);
                }
                addictivePlayerItems(data);
                slideItems();
                initPlayerItemsHandler();
                
                $("#interactionMenu").fadeIn("fast");
            },
            showVehicleMenu: (data = null) => {
                data = JSON.parse(data);
                $("#interactionMenu").empty();
                for (var i = 0; i < vehicleItems.length; i++) {
                    var info = vehicleItems[i];
                    var iconName = (info.icon) ? info.icon : "default.png";
                    var itemEl = $(`<div class="interaction_item"><div class="icon"><img src="img/interactionMenu/${iconName}"/>
                    </div><div class="text">${info.text}</div></div>
                `);
                    $("#interactionMenu").append(itemEl);
                }
                addictiveVehicleItems(data);
                slideItems();
                initVehicleItemsHandler();
                
                $("#interactionMenu").fadeIn("fast");
            },
            addItem: (iconName, text) => {
                var itemEl = $(`<div class="interaction_item"><div class="icon"><img src="img/interactionMenu/${iconName}"/>
                 </div><div class="text">${text}</div></div>
             `);
                $("#interactionMenu").append(itemEl);
            },
            addBeforeItem: (iconName, text) => {
                var itemEl = $(`<div class="interaction_item"><div class="icon"><img src="img/interactionMenu/${iconName}"/>
                 </div><div class="text">${text}</div></div>
             `);
                $("#interactionMenu").prepend(itemEl);
            },
            active: () => {
                return $("#interactionMenu").css("display") != "none";
            },
            clear: () => {
                $("#interactionMenu").empty();
            },
            hide: () => {
                $("#interactionMenu").fadeOut("fast");
                lastActionNull();
            },
            move: (x, y) => {
                //debug(`interactionMenuAPI.move: ${x} ${y}`)
                $("#interactionMenu").css("left", x + "%");
                $("#interactionMenu").css("top", y + "%");
            },
        };
        /* for tests */
        // interactionMenuAPI.showVehicleMenu();
    }
    
    render() {
        return (
            <div id="interactionMenu" style={{display: 'block'}}>
                Filling from code, defined in interaction.jsx
            </div>
        )
    }
}


/* Чтобы пункты меню не были на одном уровне от левого края. */
function slideItems() {
    var itemsCount = $("#interactionMenu .interaction_item").length;
    var slidePx = 10;
    for (var i = 0; i < parseInt((itemsCount - 1) / 2); i++) {
        var itemElA = $($("#interactionMenu .interaction_item")[1 + i]);
        var itemElB = $($("#interactionMenu .interaction_item")[itemsCount - i - 2]);
        itemElA.css("margin-left", slidePx + "px");
        itemElB.css("margin-left", slidePx + "px");
        slidePx += 10;
    }
}


function mapStateToProps(state) {
    return {
    
    };
}


const connected = connect(mapStateToProps)(Interaction);
export { connected as Interaction };