import React from 'react';
import { connect } from 'react-redux';

import '../../assets/css/speedoMetr.css';

class SpeedoMeter extends React.Component { 
    constructor(props) {
        super(props);
        this.state = {
           speedStatus: false,
           engineStatus: false,
           accumulatorStatus: false,
           oilStatus: false,
           left: false,
           right: false,
           emergency: false,
           locked: false,
           belt: false,
           rotation: 0,
           margin: 16,
           width: 360,
           lights: 0,
           mileage: 0,
           data: {}
        };

    }

    componentDidMount() {
        window.speedoMetr = {
            enable: (enable) => {
                if (enable) {
                    this.setState({ speedStatus: true });
                } else {
                    this.setState({ speedStatus: false });
                }
            },
            changeOptions: (event, options) => {
                if(event === 'VehPropHandler') {
                    this.setState({ data: options });
                } else if(event === 'MileageHandler') {
                    this.setState({ mileage: parseInt(options * 10) });
                } else if(event === 'engineStatus') {
                    this.setState({ engineStatus: options });
                } else if(event === 'accumulatorStatus') {
                    this.setState({ accumulatorStatus: options });
                } else if(event === 'OilBrokenHandler') {
                    this.setState({ oilStatus: options });
                } else if(event === 'LeftSignalHandler') {
                    this.setState({ left: options });
                } else if(event === 'RightSignalHandler') {
                    this.setState({ right: options });
                } else if(event === 'EmergencyHandler') {
                    this.setState({ emergency: options });
                } else if(event === 'LockedHandler') {
                    this.setState({ locked: options });
                } else if(event === 'BeltHandler') {
                    this.setState({ belt: options });
                } else if(event === 'LightsHandler') {
                    this.setState({ lights: options });
                }
            },
            active: () => {
                return this.state.speedStatus;
            },
        };
    }

    render() {

        var test;
        if(this.state.data.velocity < 1 && this.state.data.velocity >= 0 && this.state.data.rpm > 0.5) {
            test = 1;
        } else if(this.state.data.velocity != 0 && this.state.data.gear == 0) {
            test = 'R';
        } else if(this.state.data.velocity < 1 && this.state.data.velocity >= 0) {
            test = 'N';
        } else {
            test = this.state.data.gear;
        }

        if (parseInt(this.state.data.fuel) < 10) { 
            document.getElementById('Lstring').setAttribute('x', "10"); 
        } else if (parseInt(this.state.data.fuel) >= 10) { 
            document.getElementById('Lstring').setAttribute('x', "17"); 
        }
        
        var rotation;

        if ((this.state.data.rpm * 0.09) * 100 === 0) { 
           rotation = -27.0;
        } else {
            var speed;
            let max_rpm = 10, max_rpm_rot = 10;
            let speed2 = (this.state.data.rpm * 0.09) * 100;
            if(speed < 0.0) speed = 0.0;
            speed2 = (this.state.data.rpm * 0.09) * 100;
            if(speed2 > max_rpm) speed2 = max_rpm;
            rotation = -47.0 + ((speed2 * 251.0) / max_rpm_rot);
        }

    

        return ( 
            <div id={'speedometer'} className={this.state.engineStatus === true ? 'speedometer blackwhite animationEnd' : 'speedometer'} style={{display: this.state.speedStatus === true ? 'block' : 'none'}}>
                <div className="text">
                    <div className="speed">{parseInt(this.state.data.velocity * 1.60934)}</div>
                    <div className="border"></div>
                    <div className="kmh">
                        <div className="km">km/h</div>
                        {parseInt(this.state.mileage / 10) <= 9 ? <div id="test" className="count">00000{parseInt(this.state.mileage / 10)}</div> : null}
                        {parseInt(this.state.mileage / 10) > 9 && parseInt(this.state.mileage / 10) <= 99 ? <div id="test" className="count">0000{parseInt(this.state.mileage / 10)}</div> : null}
                        {parseInt(this.state.mileage / 10) > 99 && parseInt(this.state.mileage / 10) <= 999 ? <div id="test" className="count">000{parseInt(this.state.mileage / 10)}</div> : null}
                        {parseInt(this.state.mileage / 10) > 999 && parseInt(this.state.mileage / 10) <= 9999 ? <div id="test" className="count">00{parseInt(this.state.mileage / 10)}</div> : null}
                        {parseInt(this.state.mileage / 10) > 9999 && parseInt(this.state.mileage / 10) <= 9999 ? <div id="test" className="count">0{parseInt(this.state.mileage / 10)}</div> : null}
                        {parseInt(this.state.mileage / 10) > 99999 ? <div id="test" className="count">{parseInt(this.state.mileage / 10)}</div> : null}
                    </div>
                </div>
                
                <div className="fuelBar" style={{display: 'none'}}>
                    <div className="fuelValue"></div>
                </div>

                <img id="speedometerTest" src={require('../../assets/img/speedoMetr/line.png')} style={{transform: "rotate(" + rotation + "deg)", transformOrigin: "14.3333vh 1.4607vh"}}/> 

                <svg xmlns={'http://www.w3.org/2000/svg'} xlinkHref="http://www.w3.org/1999/xlink" viewBox={'0 0 315.98 290'}>
                    <defs>
                        <radialGradient id={'radial-gradient'} cx={'-288.43'} cy={'1068.63'} r={'219.5'}
                            gradientTransform="matrix(0, 1.02, 2.04, 0, -2037.87, 312.59)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stopColor="#087dff" stopOpacity="0.2" />
                            <stop offset="0.47" stopColor="#15a9ff" stopOpacity="0.1" />
                            <stop offset="1" stopColor="#22d4ff" stopOpacity="0" />
                        </radialGradient>
                        <radialGradient id={"radial-gradient-blackwhite"} cx={'-288.43'} cy={'1068.63'} r={'219.5'}
                            gradientTransform="matrix(0, 1.02, 2.04, 0, -2037.87, 312.59)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stopColor="#000000" stopOpacity="0.2" />
                            <stop offset="0.47" stopColor="#000000" stopOpacity="0.1" />
                            <stop offset="1" stopColor="#000000" stopOpacity="0" />
                        </radialGradient>
                        <radialGradient id={"radial-gradient-pinkpink"} cx={'-288.43'} cy={'1068.63'} r={'219.5'}
                            gradientTransform="matrix(0, 1.02, 2.04, 0, -2037.87, 312.59)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stopColor="#a450d9" stopOpacity="0.2" />
                            <stop offset="0.47" stopColor="#a450d9" stopOpacity="0.1" />
                            <stop offset="1" stopColor="#a450d9" stopOpacity="0" />
                        </radialGradient>
                    </defs>
                    <g id="Speedometer" dataName="Speedometer">
                        <g className="cls-1">
                            <rect className="cls-2" x="75.5" y="215.5" width="133" height="50" rx="5" />
                        </g>
                        <g id="Circle" dataName="Circle">
                            <path className="cls-3" d="M145,0A145,145,0,1,0,290,145,145,145,0,0,0,145,0Zm-.26,232.26A87.26,87.26,0,1,1,232,145h0a87.25,87.25,0,0,1-87.24,87.26Z" />
                            <text className="cls-4" transform="translate(41.67 192.33)">0</text>
                            <text className="cls-4" transform="translate(33.67 153.33)">1</text>
                            <text className="cls-4" transform="translate(40.67 115.33)">2</text>
                            <text className="cls-4" transform="translate(62.67 80.33)">3</text>
                            <text className="cls-4" transform="translate(95.67 56.33)">4</text>
                            <text className="cls-4" transform="translate(137.67 46.33)">5</text>
                            <text className="cls-5" transform="translate(137.67 208.33)">{test}</text>
                            <text className="cls-4" transform="translate(176.67 56.33)">6</text>
                            <text className="cls-4" transform="translate(208.67 80.33)">7</text>
                            <text className="cls-4" transform="translate(231.67 115.33)">8</text>
                            <text className="cls-4" transform="translate(239.67 153.33)">9</text>
                            <text className="cls-4" transform="translate(225.67 192.33)">10</text>
                            <rect className="cls-6" x="142.13" y="3.47" width="5" height="21.05" />
                            <rect className="cls-6" x="92" y="13.49" width="5" height="21.05" transform="translate(-2 37.99) rotate(-22.5)" />
                            <rect className="cls-6" x="49.53" y="41.94" width="5" height="21.05" transform="translate(-21.86 52.16) rotate(-45)" />
                            <rect className="cls-6" x="21.17" y="84.47" width="5" height="21.05" transform="translate(-73.15 80.51) rotate(-67.5)" />
                            <rect className="cls-6" x="3.23" y="142.62" width="21.05" height="5" />
                            <rect className="cls-6" x="13.24" y="192.75" width="21.05" height="5" transform="translate(-72.91 23.96) rotate(-22.5)" />
                            <rect className="cls-6" x="263.32" y="184.49" width="5" height="21.05" transform="translate(-16.08 365.97) rotate(-67.5)" />
                            <rect className="cls-6" x="265.23" y="142.38" width="21.05" height="5" />
                            <rect className="cls-6" x="255.2" y="92.25" width="21.05" height="5" transform="translate(-16.03 108.9) rotate(-22.5)" />
                            <rect className="cls-6" x="226.77" y="49.77" width="21.05" height="5" transform="translate(32.54 183.1) rotate(-45)" />
                            <rect className="cls-6" x="184.24" y="21.43" width="21.05" height="5" transform="translate(98.13 194.71) rotate(-67.5)" />
                            <rect className="cls-6" x="116.77" y="5.62" width="3" height="15" transform="translate(-0.29 23.33) rotate(-11.25)" />
                            <rect className="cls-6" x="163.76" y="11.64" width="15" height="3" transform="translate(124.96 178.55) rotate(-78.75)" />
                            <rect className="cls-6" x="68.32" y="25.81" width="3" height="15" transform="translate(-6.74 44.4) rotate(-33.75)" />
                            <rect className="cls-6" x="31.27" y="62.98" width="3" height="15" transform="translate(-44.04 58.57) rotate(-56.25)" />
                            <rect className="cls-6" x="11.29" y="111.5" width="3" height="15" transform="translate(-106.42 108.33) rotate(-78.75)" />
                            <rect className="cls-6" x="5.38" y="169.99" width="15" height="3" transform="translate(-33.21 5.81) rotate(-11.25)" />
                            <rect className="cls-6" x="275.22" y="163.49" width="3" height="15" transform="translate(55.03 409.04) rotate(-78.75)" />
                            <rect className="cls-6" x="269.11" y="117.02" width="15" height="3" transform="translate(-17.81 56.24) rotate(-11.25)" />
                            <rect className="cls-6" x="248.94" y="68.57" width="15" height="3" transform="translate(4.29 154.28) rotate(-33.75)" />
                            <rect className="cls-6" x="211.77" y="31.53" width="15" height="3" transform="matrix(0.56, -0.83, 0.83, 0.56, 69.98, 196.99)" />
                            <text className="cls-7" transform="translate(126.17 89)">RPM</text>
                            <text className="cls-8" transform="translate(126.17 99)">X1000</text>
                        </g>
                        {this.state.engineStatus === false ? 
                        <g id="Doors" className={'disabledElement'} dataName="Doors">
                            <path className="cls-9" d="M83.15,269.92,79.39,274H75.83c-1.73,0-3.17.9-3.17,2.74v5.06A3.17,3.17,0,0,0,75.78,285h3.56L83,288.68c.6.18,1,.18,1.1,0,.29-.37-.55-1.6-2.53-3.66h7.75a3.3,3.3,0,0,0,3.31-3.23v-5.06c0-1.84-1.58-2.74-3.31-2.74H81.61c2.08-2.31,3-3.67,2.65-4.07C84.12,269.78,83.69,269.78,83.15,269.92Zm4.48,14H81.77c.19,0,.4-1.11.6-1.11H87C87.17,282.81,87.37,283.92,87.63,283.92Zm1.66-4.6v3.36l-1.1-.61v-5.48l1.1-.61Zm-7.76-2.66V282l-2.22.68V276Zm6.13-1.57c-.23,0-.45,1.1-.67,1.1H82.37c-.2,0-.41-1.1-.6-1.1Z" />
                        </g> : null}
                        {this.state.engineStatus === true ? 
                        <g id="Doors" className={this.state.locked === true ? 'active' : 'activElement'} dataName="Doors">
                            <path className="cls-9" d="M83.15,269.92,79.39,274H75.83c-1.73,0-3.17.9-3.17,2.74v5.06A3.17,3.17,0,0,0,75.78,285h3.56L83,288.68c.6.18,1,.18,1.1,0,.29-.37-.55-1.6-2.53-3.66h7.75a3.3,3.3,0,0,0,3.31-3.23v-5.06c0-1.84-1.58-2.74-3.31-2.74H81.61c2.08-2.31,3-3.67,2.65-4.07C84.12,269.78,83.69,269.78,83.15,269.92Zm4.48,14H81.77c.19,0,.4-1.11.6-1.11H87C87.17,282.81,87.37,283.92,87.63,283.92Zm1.66-4.6v3.36l-1.1-.61v-5.48l1.1-.61Zm-7.76-2.66V282l-2.22.68V276Zm6.13-1.57c-.23,0-.45,1.1-.67,1.1H82.37c-.2,0-.41-1.1-.6-1.1Z" />
                        </g> : null}
                        {this.state.engineStatus === true ? 
                        <g id="High_Beam" className={this.state.lights === 3 ? 'active' : 'activElement'} dataName="High_Beam">
                            <polygon className="cls-10" points="108.22 274.49 101.14 274.49 101.14 273.5 108.51 273.5 108.51 273.51 108.22 274.49" />
                            <polygon className="cls-10" points="107.67 277.44 101.14 277.44 101.14 276.44 107.81 276.44 107.67 277.44" />
                            <polygon className="cls-10" points="107.57 279.39 101.14 279.39 101.14 280.38 107.58 280.38 107.57 279.39" />
                            <polygon className="cls-10" points="107.74 282.33 101.14 282.33 101.14 283.32 107.91 283.32 107.74 282.33" />
                            <polygon className="cls-10" points="108.51 285.68 108.51 286.27 101.14 286.27 101.14 285.28 108.37 285.28 108.51 285.68" />
                            <path className="cls-10" d="M117.48,275.21a7.93,7.93,0,0,0-6.29-1.31,15.34,15.34,0,0,0,0,11.93,7.78,7.78,0,0,0,5.94-1.07c.8-.53,3.37-2.23,3.37-4.88S118.33,275.8,117.48,275.21Z" />
                        </g> : null}
                        {this.state.engineStatus === false ? 
                        <g id="High_Beam" className={'disabledElement'} dataName="High_Beam">
                            <polygon className="cls-10" points="108.22 274.49 101.14 274.49 101.14 273.5 108.51 273.5 108.51 273.51 108.22 274.49" />
                            <polygon className="cls-10" points="107.67 277.44 101.14 277.44 101.14 276.44 107.81 276.44 107.67 277.44" />
                            <polygon className="cls-10" points="107.57 279.39 101.14 279.39 101.14 280.38 107.58 280.38 107.57 279.39" />
                            <polygon className="cls-10" points="107.74 282.33 101.14 282.33 101.14 283.32 107.91 283.32 107.74 282.33" />
                            <polygon className="cls-10" points="108.51 285.68 108.51 286.27 101.14 286.27 101.14 285.28 108.37 285.28 108.51 285.68" />
                            <path className="cls-10" d="M117.48,275.21a7.93,7.93,0,0,0-6.29-1.31,15.34,15.34,0,0,0,0,11.93,7.78,7.78,0,0,0,5.94-1.07c.8-.53,3.37-2.23,3.37-4.88S118.33,275.8,117.48,275.21Z" />
                        </g> : null}
                        {this.state.engineStatus === true ? 
                        <g id="Parking_Light" className={this.state.lights === 1 ? 'active' : 'activElement'} dataName="Parking_Light">
                            <path className="cls-10" d="M143.12,275.17a6.14,6.14,0,0,0-5-1.11,13.57,13.57,0,0,0-.9,5,13.23,13.23,0,0,0,1,5.1,5.85,5.85,0,0,0,4.73-.9c.64-.44,2.7-1.87,2.68-4.11S143.79,275.69,143.12,275.17Z" />
                            <path className="cls-10" d="M135.58,274.73a3.58,3.58,0,0,0-.16.56c0,.19-.09.39-.12.58l-6-1.14.2-1.23Z" />
                            <path className="cls-10" d="M129.27,282.28l.2,1.15,6-1.15a2.7,2.7,0,0,1-.18-.57,2.5,2.5,0,0,0-.13-.57Z" />
                            <path className="cls-10" d="M135.17,277.94a5.23,5.23,0,0,0,0,.56c0,.21,0,.41,0,.62h-6.12V277.9Z" />
                            <path className="cls-10" d="M149.56,275.17a6.15,6.15,0,0,1,5-1.11,13.56,13.56,0,0,1,0,10.06,5.86,5.86,0,0,1-4.73-.9c-.64-.44-2.69-1.87-2.69-4.11A4.94,4.94,0,0,1,149.56,275.17Z" />
                            <path className="cls-10" d="M157,274.73a3,3,0,0,0,.15.56,5.43,5.43,0,0,1,.13.58l5.88-1.14-.2-1.23Z" />
                            <path className="cls-10" d="M163.28,282.28l-.2,1.15L157,282.28a5.6,5.6,0,0,0,.17-.57,3.43,3.43,0,0,0,.12-.57Z" />
                            <path className="cls-10" d="M157.38,277.94v.56c0,.21,0,.41,0,.62h6.12V277.9Z" />
                        </g> : null}
                        {this.state.engineStatus === false ? 
                        <g id="Parking_Light" className={'disabledElement'} dataName="Parking_Light">
                            <path className="cls-10" d="M143.12,275.17a6.14,6.14,0,0,0-5-1.11,13.57,13.57,0,0,0-.9,5,13.23,13.23,0,0,0,1,5.1,5.85,5.85,0,0,0,4.73-.9c.64-.44,2.7-1.87,2.68-4.11S143.79,275.69,143.12,275.17Z" />
                            <path className="cls-10" d="M135.58,274.73a3.58,3.58,0,0,0-.16.56c0,.19-.09.39-.12.58l-6-1.14.2-1.23Z" />
                            <path className="cls-10" d="M129.27,282.28l.2,1.15,6-1.15a2.7,2.7,0,0,1-.18-.57,2.5,2.5,0,0,0-.13-.57Z" />
                            <path className="cls-10" d="M135.17,277.94a5.23,5.23,0,0,0,0,.56c0,.21,0,.41,0,.62h-6.12V277.9Z" />
                            <path className="cls-10" d="M149.56,275.17a6.15,6.15,0,0,1,5-1.11,13.56,13.56,0,0,1,0,10.06,5.86,5.86,0,0,1-4.73-.9c-.64-.44-2.69-1.87-2.69-4.11A4.94,4.94,0,0,1,149.56,275.17Z" />
                            <path className="cls-10" d="M157,274.73a3,3,0,0,0,.15.56,5.43,5.43,0,0,1,.13.58l5.88-1.14-.2-1.23Z" />
                            <path className="cls-10" d="M163.28,282.28l-.2,1.15L157,282.28a5.6,5.6,0,0,0,.17-.57,3.43,3.43,0,0,0,.12-.57Z" />
                            <path className="cls-10" d="M157.38,277.94v.56c0,.21,0,.41,0,.62h6.12V277.9Z" />
                        </g> : null}
                        {this.state.engineStatus === true ? 
                        <g id="Low_Beam" className={this.state.lights === 2 ? 'active' : 'activElement'} dataName="Low_Beam">
                            <path className="cls-10" d="M188.11,275A8.27,8.27,0,0,0,182.3,274a11,11,0,0,0-1.12,4.72,10.81,10.81,0,0,0,1.14,4.89,8.16,8.16,0,0,0,5.5-.86c.73-.42,3.12-1.78,3.11-3.91S188.86,275.48,188.11,275Z" />
                            <path className="cls-10" d="M179.48,274.44l-6.34,1.61-.2-.78,7-1.77a2.82,2.82,0,0,0-.21.38A3.83,3.83,0,0,0,179.48,274.44Z" />
                            <path className="cls-10" d="M179.06,276.83l-5.92,1.46-.2-.75,6.21-1.58a3.16,3.16,0,0,1-.1.5A2.12,2.12,0,0,0,179.06,276.83Z" />
                            <path className="cls-10" d="M178.89,278.41l-5.95,1.49.24.83,5.77-1.51v-.47C179,278.59,178.89,278.52,178.89,278.41Z" />
                            <path className="cls-10" d="M179.2,280.73l-5.86,1.49.18.77,5.91-1.51-.11-.34C179.27,281,179.24,280.87,179.2,280.73Z" />
                            <path className="cls-10" d="M180,283.32a2.71,2.71,0,0,0,.28.41l-6.67,1.68-.2-.77,6.46-1.64C179.92,283.11,180,283.21,180,283.32Z" />
                        </g> : null}
                        {this.state.engineStatus === false ? 
                        <g id="Low_Beam" className={'disabledElement'} dataName="Low_Beam">
                            <path className="cls-10" d="M188.11,275A8.27,8.27,0,0,0,182.3,274a11,11,0,0,0-1.12,4.72,10.81,10.81,0,0,0,1.14,4.89,8.16,8.16,0,0,0,5.5-.86c.73-.42,3.12-1.78,3.11-3.91S188.86,275.48,188.11,275Z" />
                            <path className="cls-10" d="M179.48,274.44l-6.34,1.61-.2-.78,7-1.77a2.82,2.82,0,0,0-.21.38A3.83,3.83,0,0,0,179.48,274.44Z" />
                            <path className="cls-10" d="M179.06,276.83l-5.92,1.46-.2-.75,6.21-1.58a3.16,3.16,0,0,1-.1.5A2.12,2.12,0,0,0,179.06,276.83Z" />
                            <path className="cls-10" d="M178.89,278.41l-5.95,1.49.24.83,5.77-1.51v-.47C179,278.59,178.89,278.52,178.89,278.41Z" />
                            <path className="cls-10" d="M179.2,280.73l-5.86,1.49.18.77,5.91-1.51-.11-.34C179.27,281,179.24,280.87,179.2,280.73Z" />
                            <path className="cls-10" d="M180,283.32a2.71,2.71,0,0,0,.28.41l-6.67,1.68-.2-.77,6.46-1.64C179.92,283.11,180,283.21,180,283.32Z" />
                        </g> : null}
                        {this.state.engineStatus === false ?
                        <g id="Safety_Belt" className={'disabledElement'} dataName="Safety_Belt">
                            <path className="cls-10" d="M202.07,281.22l-.67.32a16.51,16.51,0,0,0,.79,2.5c.68,1.8,1.06,2.12,1.41,2.18a.91.91,0,0,0,.46-.07c-.41-1.13-.8-2.35-1.2-3.52a5.34,5.34,0,0,1,2.19-.45,5.48,5.48,0,0,1,2.25.45c-.35,1.14-.72,2.27-1.07,3.41a.81.81,0,0,0,.5.1c.48-.07.75-.73,1.22-2.09.35-1,.61-1.78.78-2.39a9,9,0,0,0-1.34-.51A7.84,7.84,0,0,0,202.07,281.22Z" />
                            <path className="cls-10" d="M204.36,280.12a9.94,9.94,0,0,1,3.77.64c.27.1.51.2.75.31a3.17,3.17,0,0,0,.07-.69,15.77,15.77,0,0,0-.27-1.95c-.38-2.12-.56-3.19-1-3.57a2.39,2.39,0,0,0-.59-.36c-1.82,2.05-3.63,4.09-5.44,6.15A9,9,0,0,1,204.36,280.12Z" />
                            <path className="cls-10" d="M205.51,273.67a1.23,1.23,0,0,1,0-.66.88.88,0,0,1,.3-.45,1.93,1.93,0,0,0,.66-1.47,1.37,1.37,0,1,0-2.71,0,1.89,1.89,0,0,0,.65,1.44,1.06,1.06,0,0,1,.32.5,1.38,1.38,0,0,1,0,.64c-.27.7-1.43.48-2.22,1.2-.59.52-.73,1.58-1.06,3.69-.09.66-.14,1.06-.16,1.47l5.16-5.68C206,274.15,205.66,274,205.51,273.67Z" />
                            <path className="cls-10" d="M209.25,272.1l-.45-.46-2.36,2.63a6.2,6.2,0,0,1,.69.23Z" />
                            <path className="cls-10" d="M208.88,281.07c0,.22-.09.44-.15.65a10.31,10.31,0,0,1,2.39,1.55l.38-.5A10.8,10.8,0,0,0,208.88,281.07Z" />
                            <path className="cls-10" d="M201.28,280l-2.53,2.81.45.49a8.13,8.13,0,0,1,2.2-1.77,7.8,7.8,0,0,1-.12-1.27Z" />
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Safety_Belt" className={this.state.belt === false ? 'active' : 'activElement'} dataName="Safety_Belt">
                            <path className="cls-10" d="M202.07,281.22l-.67.32a16.51,16.51,0,0,0,.79,2.5c.68,1.8,1.06,2.12,1.41,2.18a.91.91,0,0,0,.46-.07c-.41-1.13-.8-2.35-1.2-3.52a5.34,5.34,0,0,1,2.19-.45,5.48,5.48,0,0,1,2.25.45c-.35,1.14-.72,2.27-1.07,3.41a.81.81,0,0,0,.5.1c.48-.07.75-.73,1.22-2.09.35-1,.61-1.78.78-2.39a9,9,0,0,0-1.34-.51A7.84,7.84,0,0,0,202.07,281.22Z" />
                            <path className="cls-10" d="M204.36,280.12a9.94,9.94,0,0,1,3.77.64c.27.1.51.2.75.31a3.17,3.17,0,0,0,.07-.69,15.77,15.77,0,0,0-.27-1.95c-.38-2.12-.56-3.19-1-3.57a2.39,2.39,0,0,0-.59-.36c-1.82,2.05-3.63,4.09-5.44,6.15A9,9,0,0,1,204.36,280.12Z" />
                            <path className="cls-10" d="M205.51,273.67a1.23,1.23,0,0,1,0-.66.88.88,0,0,1,.3-.45,1.93,1.93,0,0,0,.66-1.47,1.37,1.37,0,1,0-2.71,0,1.89,1.89,0,0,0,.65,1.44,1.06,1.06,0,0,1,.32.5,1.38,1.38,0,0,1,0,.64c-.27.7-1.43.48-2.22,1.2-.59.52-.73,1.58-1.06,3.69-.09.66-.14,1.06-.16,1.47l5.16-5.68C206,274.15,205.66,274,205.51,273.67Z" />
                            <path className="cls-10" d="M209.25,272.1l-.45-.46-2.36,2.63a6.2,6.2,0,0,1,.69.23Z" />
                            <path className="cls-10" d="M208.88,281.07c0,.22-.09.44-.15.65a10.31,10.31,0,0,1,2.39,1.55l.38-.5A10.8,10.8,0,0,0,208.88,281.07Z" />
                            <path className="cls-10" d="M201.28,280l-2.53,2.81.45.49a8.13,8.13,0,0,1,2.2-1.77,7.8,7.8,0,0,1-.12-1.27Z" />
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Oil" className={this.state.oilStatus === true ? 'active': 'activElement'} dataName="Oil">
                            <path className="cls-9" d="M212.32,106.37l-.53-.57a.54.54,0,0,0-.56-.17l-7.14,2h-9.43a.63.63,0,0,0-.43.19l-1.14,1.28a.57.57,0,0,1-.6.16l-7.7-2.53a.58.58,0,0,0-.67.27l-.76,1.31a.67.67,0,0,0,.17.87h.06a.56.56,0,0,0,.78-.06l0,0,.26-.38a.6.6,0,0,1,.72-.19.63.63,0,0,1,.28.28l4.67,9a.54.54,0,0,0,.5.33h13.31a.62.62,0,0,0,.58-.63V114a.62.62,0,0,1,.46-.62l6.07-1.64a.59.59,0,0,0,.27-.14l.8-.8a.66.66,0,0,0,.19-.5v-3.43A.63.63,0,0,0,212.32,106.37Zm-9.11,5.78v3.78a.61.61,0,0,1-.59.63h-11a.58.58,0,0,1-.51-.37l-2.88-5.56a.62.62,0,0,1,.68-.91l4,1.27a.58.58,0,0,0,.58-.17l1.35-1.45a.57.57,0,0,1,.41-.19h7.36a.63.63,0,0,1,.59.64Zm7.81-2.28-.34.38-5.28,1.33a.61.61,0,0,1-.72-.46.81.81,0,0,1,0-.16v-1.41a.63.63,0,0,1,.45-.62l5.18-1.42a.6.6,0,0,1,.71.46.42.42,0,0,1,0,.16Z" />
                            <path className="cls-9" d="M196.21,107.08h6.2a.34.34,0,0,0,.33-.34v-.88a.34.34,0,0,0-.32-.36h-6.21a.33.33,0,0,0-.33.34v.85a.33.33,0,0,0,.27.38Z" />
                            <path className="cls-9" d="M184.38,112.81a10.61,10.61,0,0,1-.85-2.21,13.67,13.67,0,0,1-.86,2.28,4.94,4.94,0,0,0-.82,2A1.75,1.75,0,0,0,183.2,117l.22,0a1.62,1.62,0,0,0,.44,0,1.84,1.84,0,0,0,1.36-2.25l0-.13A5.78,5.78,0,0,0,184.38,112.81Z" />
                        </g> : null}
                        {this.state.engineStatus === false ?
                        <g id="Oil" className={'disabledElement'} dataName="Oil">
                            <path className="cls-9" d="M212.32,106.37l-.53-.57a.54.54,0,0,0-.56-.17l-7.14,2h-9.43a.63.63,0,0,0-.43.19l-1.14,1.28a.57.57,0,0,1-.6.16l-7.7-2.53a.58.58,0,0,0-.67.27l-.76,1.31a.67.67,0,0,0,.17.87h.06a.56.56,0,0,0,.78-.06l0,0,.26-.38a.6.6,0,0,1,.72-.19.63.63,0,0,1,.28.28l4.67,9a.54.54,0,0,0,.5.33h13.31a.62.62,0,0,0,.58-.63V114a.62.62,0,0,1,.46-.62l6.07-1.64a.59.59,0,0,0,.27-.14l.8-.8a.66.66,0,0,0,.19-.5v-3.43A.63.63,0,0,0,212.32,106.37Zm-9.11,5.78v3.78a.61.61,0,0,1-.59.63h-11a.58.58,0,0,1-.51-.37l-2.88-5.56a.62.62,0,0,1,.68-.91l4,1.27a.58.58,0,0,0,.58-.17l1.35-1.45a.57.57,0,0,1,.41-.19h7.36a.63.63,0,0,1,.59.64Zm7.81-2.28-.34.38-5.28,1.33a.61.61,0,0,1-.72-.46.81.81,0,0,1,0-.16v-1.41a.63.63,0,0,1,.45-.62l5.18-1.42a.6.6,0,0,1,.71.46.42.42,0,0,1,0,.16Z" />
                            <path className="cls-9" d="M196.21,107.08h6.2a.34.34,0,0,0,.33-.34v-.88a.34.34,0,0,0-.32-.36h-6.21a.33.33,0,0,0-.33.34v.85a.33.33,0,0,0,.27.38Z" />
                            <path className="cls-9" d="M184.38,112.81a10.61,10.61,0,0,1-.85-2.21,13.67,13.67,0,0,1-.86,2.28,4.94,4.94,0,0,0-.82,2A1.75,1.75,0,0,0,183.2,117l.22,0a1.62,1.62,0,0,0,.44,0,1.84,1.84,0,0,0,1.36-2.25l0-.13A5.78,5.78,0,0,0,184.38,112.81Z" />
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Battery" className={this.state.accumulatorStatus === true ? 'active' : 'activElement'}  dataName="Battery">
                            <path className="cls-11" d="M197.09,135.39H213.8a.74.74,0,0,1,.7.78v11.55a.74.74,0,0,1-.7.78H197.09a.74.74,0,0,1-.7-.78V136.17A.74.74,0,0,1,197.09,135.39Z" />
                            <rect className="cls-9" x="210.09" y="140.04" width="0.61" height="3.88" />
                            <path className="cls-9" d="M199.21,141.46h3.47a.21.21,0,0,1,.2.22v1.13a.21.21,0,0,1-.2.22h-3.47a.21.21,0,0,1-.2-.22v-1.13A.21.21,0,0,1,199.21,141.46Z" />
                            <rect className="cls-9" x="208.48" y="141.58" width="3.48" height="0.68" />
                        </g> : null}
                        {this.state.engineStatus === false ?
                        <g id="Battery" className={'disabledElement'} dataName="Battery">
                            <path className="cls-11" d="M197.09,135.39H213.8a.74.74,0,0,1,.7.78v11.55a.74.74,0,0,1-.7.78H197.09a.74.74,0,0,1-.7-.78V136.17A.74.74,0,0,1,197.09,135.39Z" />
                            <rect className="cls-9" x="210.09" y="140.04" width="0.61" height="3.88" />
                            <path className="cls-9" d="M199.21,141.46h3.47a.21.21,0,0,1,.2.22v1.13a.21.21,0,0,1-.2.22h-3.47a.21.21,0,0,1-.2-.22v-1.13A.21.21,0,0,1,199.21,141.46Z" />
                            <rect className="cls-9" x="208.48" y="141.58" width="3.48" height="0.68" />
                        </g> : null}
                        {this.state.engineStatus === false ?
                        <g id="Engine" className={'disabledElement'} dataName="Engine">
                            <path className="cls-9" d="M204.45,168.89h-2.28c-.22,0-.19,0-.19-.12v-.64h-2.23a.51.51,0,0,1-.33-.13l-1-1.21a.6.6,0,0,0-.36-.16h-2v-.84h.68a.62.62,0,0,0,.27,0c1-.58.46-1.55-.38-1.55h-6.72a.89.89,0,0,0-.28.07c-1,.51-.46,1.48.39,1.48h.56v.78h-1.77l-1,1.39a.39.39,0,0,1-.34.16H185.8a.18.18,0,0,0-.19.15v1.79h-1v-1.58a.34.34,0,0,0-.07-.22c-.7-.81-1.91-.4-1.91.3v6.9a.38.38,0,0,0,.07.21c.68.82,1.91.36,1.91-.29V173.9h1v1.5h0l2.11,1.88a.32.32,0,0,0,.24.09h2.95l1.47,1.56h6a.54.54,0,0,0,.32-.13l2.3-2.09v-.11h3.47Zm-2,6.21H199v1l-.12.25-1.14,1a.2.2,0,0,1-.15.06h-4.36l-1.46-1.55h-3a.4.4,0,0,1-.23-.08l-.89-.77-.08-.18v-5.55h.93c.14,0,.27,0,.33-.08l.76-.94a.58.58,0,0,1,.38-.15h7.24l1.2,1.4a.55.55,0,0,0,.36.15H200v.61c0,.17,0,.15.23.15h2.26Z" />
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Engine" className={this.state.engineStatus === true ? 'activElement': 'active'} dataName="Engine">
                            <path className="cls-9" d="M204.45,168.89h-2.28c-.22,0-.19,0-.19-.12v-.64h-2.23a.51.51,0,0,1-.33-.13l-1-1.21a.6.6,0,0,0-.36-.16h-2v-.84h.68a.62.62,0,0,0,.27,0c1-.58.46-1.55-.38-1.55h-6.72a.89.89,0,0,0-.28.07c-1,.51-.46,1.48.39,1.48h.56v.78h-1.77l-1,1.39a.39.39,0,0,1-.34.16H185.8a.18.18,0,0,0-.19.15v1.79h-1v-1.58a.34.34,0,0,0-.07-.22c-.7-.81-1.91-.4-1.91.3v6.9a.38.38,0,0,0,.07.21c.68.82,1.91.36,1.91-.29V173.9h1v1.5h0l2.11,1.88a.32.32,0,0,0,.24.09h2.95l1.47,1.56h6a.54.54,0,0,0,.32-.13l2.3-2.09v-.11h3.47Zm-2,6.21H199v1l-.12.25-1.14,1a.2.2,0,0,1-.15.06h-4.36l-1.46-1.55h-3a.4.4,0,0,1-.23-.08l-.89-.77-.08-.18v-5.55h.93c.14,0,.27,0,.33-.08l.76-.94a.58.58,0,0,1,.38-.15h7.24l1.2,1.4a.55.55,0,0,0,.36.15H200v.61c0,.17,0,.15.23.15h2.26Z" />
                        </g> : null}
                        <text className="cls-12" transform="translate(290.93 175.56)">
                            {parseInt(this.state.data.fuel) > 5 ? parseInt(this.state.data.fuel) : parseInt(this.state.data.fuel)}
                            <tspan id="Lstring" className="cls-13" x="17.09" y="0">L</tspan>
                        </text>
                        <g id="Fuel" dataName="Fuel">
                            <g id="symbols">
                                <path className="cls-6" d="M309.55,185.08l.77-.21c.22-.06.25-.15.19-.35-.92-2.89-2.49-5.07-5.29-6-.21-.07-.34,0-.42.22a1.68,1.68,0,0,0-.08.59.54.54,0,0,0,.18.44,8.17,8.17,0,0,1,3.2,6.38,41.44,41.44,0,0,0,1.44,7.32,1.81,1.81,0,0,1-3.45,1.11A2,2,0,0,1,306,194v-5.23a2.47,2.47,0,0,0-2.44-2.47h-.28v-6.58a1.21,1.21,0,0,0-1.21-1.21h-8.27a1.21,1.21,0,0,0-1.21,1.21h0v17.91a.34.34,0,0,1-.34.34h-.38a.34.34,0,0,0-.35.33h0v.8a.35.35,0,0,0,.34.35H304a.35.35,0,0,0,.35-.35v-.8A.35.35,0,0,0,304,198h-.37a.35.35,0,0,1-.35-.34h0v-9.95h.21a1.07,1.07,0,0,1,1.11,1.05.08.08,0,0,1,0,0V194a3.19,3.19,0,1,0,6.23-1,37.63,37.63,0,0,1-1.46-7.78A.2.2,0,0,1,309.55,185.08Zm-7.65.67a.34.34,0,0,1-.34.34h-7.13a.34.34,0,0,1-.34-.34v-5.42a.35.35,0,0,1,.34-.35h7.13a.35.35,0,0,1,.34.35Z" />
                            </g>
                        </g>
                        {this.state.engineStatus === false ?
                        <g id="Right_Arrow" className={'disabledElement'} dataName="Right_Arrow">
                            <path className="cls-14" d="M212.38,233.32l17,6.87a.81.81,0,0,1,.41,1,.74.74,0,0,1-.41.47l-17,6.82a.66.66,0,0,1-.85-.38l0-.09a1.13,1.13,0,0,1,0-.26V234.1a.76.76,0,0,1,.65-.83A.59.59,0,0,1,212.38,233.32Z"/>
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Right_Arrow" className={this.state.right === true ? 'active': 'activElement'} dataName="Right_Arrow">
                            <path className="cls-14" d="M212.38,233.32l17,6.87a.81.81,0,0,1,.41,1,.74.74,0,0,1-.41.47l-17,6.82a.66.66,0,0,1-.85-.38l0-.09a1.13,1.13,0,0,1,0-.26V234.1a.76.76,0,0,1,.65-.83A.59.59,0,0,1,212.38,233.32Z"/>
                        </g> : null}
                        {this.state.engineStatus === false ?
                        <g id="Left_Arrow" className={'disabledElement'} dataName="Left_Arrow">
                            <path className="cls-14" d="M71.61,248.47l-17-6.87a.81.81,0,0,1-.41-1,.74.74,0,0,1,.41-.47l17-6.83a.69.69,0,0,1,.87.42s0,0,0,.06a1.13,1.13,0,0,1,0,.26v13.65a.74.74,0,0,1-.64.83A.69.69,0,0,1,71.61,248.47Z"/>
                        </g> : null}
                        {this.state.engineStatus === true ?
                        <g id="Left_Arrow" className={this.state.left === true ? 'active': 'activElement'} dataName="Left_Arrow">
                            <path className="cls-14" d="M71.61,248.47l-17-6.87a.81.81,0,0,1-.41-1,.74.74,0,0,1,.41-.47l17-6.83a.69.69,0,0,1,.87.42s0,0,0,.06a1.13,1.13,0,0,1,0,.26v13.65a.74.74,0,0,1-.64.83A.69.69,0,0,1,71.61,248.47Z"/>
                        </g> : null}
                        <image width="94" height="94" transform="translate(95.5 94.5)" style={{opacity: '0.6'}} xlinkHref={require('../../assets/img/speedoMetr/red_circle.png')}/> />
                    </g>
                </svg>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {

    };
};

const connected = connect(mapStateToProps)(SpeedoMeter);
export { connected as SpeedoMeter }; 